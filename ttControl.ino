/* code credits: 
Ben Cox 2019 
Rotates a turntable and triggers photos for photogrammetry

*/

#include <Wire.h>                 // i2c comms
#include <LiquidCrystal_I2C.h>    // for the 1602 display


// set the LCD address to 0x27 for a 16 chars 2 line display
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

// define the IO pins
//#define rLED_pin 1
//#define bLED_pin 2
//#define gLED_pin 3
//#define Fan_Pin 5
#define Buzzer_Pin 6
#define Button1_Pin 7
//#define Button2_Pin 8
#define Relay1_Pin 9
//#define Relay2_Pin 10
//#define Relay3_Pin 11
//#define Relay4_Pin 12
#define LedPin 13


// for debounce
unsigned long previousMillis = 0; // Previous time in ms
unsigned long millisPassed = 0;  // Current time in ms

// not used
//int x = 0;
//int row = 0;


char version[16] = "ttControl_v0.0.1"; // 16 character version to display at boot

// config settings - edit settings will be in the UI eventually....
int relayOnTime = 120;       // seconds for a complete rotation - user configurable
bool capturePics = 1;     // 0 = video, 1 = stills - how to trigger camera 
int picsTotal = 100;        // set the "resolution" of the scan
int scanObjectBottom = 1;     // >0 = prompt to turn the object over
int camPositions = 3;       //how many camera angles to capture

// globals to keep track of the scan progress
int camPosition = 0;        // keeps track of camera positions
int picInterval;          // milliseconds, calculated - time between pics
int picsTaken;             // used to calculate the rotation progress
int totalPicsTaken;        // used to calculate the total progress
//int picsPerRevolution;      // calculated from total pics



//************ Setup Function *********************

void setup() {
    Serial.begin(9600);
    //--------- Prepare the pins ----------------
 
    pinMode(LedPin, OUTPUT);       // Prepare the on-board LED
    pinMode(Relay1_Pin, OUTPUT);
    //pinMode(Relay2_Pin, OUTPUT);    // 
    //pinMode(Relay3_Pin, OUTPUT);
    //pinMode(Relay4_Pin, OUTPUT);
    //pinMode(Buzzer_Pin, OUTPUT);
    //pinMode(Fan_Pin, OUTPUT);
    pinMode(Button1_Pin, INPUT_PULLUP);
    
    digitalWrite(Relay1_Pin, LOW);  // Relays off during setup
    //digitalWrite(Relay2_Pin, LOW);  // Relays off during setup
    //digitalWrite(Relay3_Pin, LOW);  // Relays off during setup
    //digitalWrite(Relay4_Pin, LOW);  // Relays off during setup

    // prepare the LCD and diplay the start screen
    lcd.init();
    lcd.backlight();
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("3D Scanner");
    lcd.setCursor(0,1);
    lcd.print(version);
    delay(5000);
}


void blink(int blinks, int blinkDelay, int blinkPin){
    while(blink < blinks){
        digitalWrite(blinkPin, HIGH);
        delay(blinkDelay);
        digitalWrite(blinkPin, LOW);
        delay(blinkDelay);
    }
    
}

//**********Buzzer Beep Function **************
void beep(int delay_time){
  analogWrite(Buzzer_Pin, 20);      // PWM signal to generate beep tone
  delay(delay_time);          // wait for a delayms ms
  analogWrite(Buzzer_Pin, 0);  // 0 turns it off
  delay(delay_time);          // wait for a delayms ms  

} 

void test_relays() {
    
    digitalWrite(Relay1_Pin, HIGH);   // Turn On the relay 
    delay(500);
    digitalWrite(Relay1_Pin, LOW);    // Turn Off the relay

    delay(100);
    
//     digitalWrite(Relay2_Pin, HIGH);   // Turn On the relay 
//     delay(500);
//     digitalWrite(Relay2_Pin, LOW);    // Turn Off the relay
//     
//     delay(100);
//     
//     digitalWrite(Relay3_Pin, HIGH);   // Turn On the relay 
//     delay(500);
//     digitalWrite(Relay3_Pin, LOW);    // Turn Off the relay
//     
//     delay(100);
//     
//     digitalWrite(Relay4_Pin, HIGH);   // Turn On the relay 
//     delay(500);
//     digitalWrite(Relay4_Pin, LOW);    // Turn Off the relay
    
}

void buttonWait(int buttonPin){
  int buttonState = 0;
  Serial.print("waiting for button press on pin ");
  Serial.println(buttonPin);
  while(1){
    blink(1,100, LedPin);
    buttonState = digitalRead(buttonPin);
    if (buttonState == LOW) {
      return;
    }
  }
}

bool connectSerial(){                // confirms connection to the python script
//     char test[3] = "@OK";
//     String test = "@OK";
    String responseString;
    char responseChar[3];

    Serial.println("$hello");
    while (!Serial.available()){
        blink(1, 100, LedPin);
    }
    responseString = Serial.readString();
//     Serial.println("looking for: @OK ");
//     Serial.println(test);
//     Serial.print("length is: ");
//     Serial.println(responseString.length());
//     Serial.print("and got: ");
//     Serial.println(responseString);
//     Serial.print("remove extra characters: ");
    responseString.remove(3);
//     Serial.println(responseString);
//     Serial.print("length is: ");
//     Serial.println(responseString.length());
//     Serial.print("convert to char :");
    responseString.toCharArray(responseChar, 4);  // don't know why this needs to be length + 1
    Serial.print("Response from camera: ");
    Serial.println(responseChar);
//     Serial.print(responseChar[0]);
//     Serial.print(responseChar[1]);
//     Serial.println(responseChar[2]);

    if (strcmp(responseChar, "@OK") == 0){
        Serial.println("connection OK, continue");
        return(true);
    }
    else{
        Serial.println("no response from serial port");
        return(false);
   }
}


bool getConfirm(){      // general comfirmation that the python script received a message
    String responseString;
    char responseChar[3];
    
    while (!Serial.available()){
        blink(1, 100, LedPin);
    }
    responseString = Serial.readString();
    responseString.remove(3);
    responseString.toCharArray(responseChar, 4);  // don't know why this needs to be length + 1

    lcd.clear();
    lcd.setCursor(4,0);
    lcd.print(responseChar);
    if (strcmp(responseChar, "@OK") == 0){
        Serial.println("response OK, continue");
        return(true);
    }
    else{
        blink(10,100, LedPin);
        Serial.print("Response was :");
        Serial.println(responseChar);
        return(false);
    }
}

void showSettings(){

    //TODO: padding undisplayed digits
    
    Serial.println("Show Settings");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("pics:");
    lcd.setCursor(6,0);
    lcd.print(picsTotal);
    lcd.setCursor(0,1);
    lcd.print("angles:");
    lcd.setCursor(8,1);
    lcd.print(camPositions);
    
    lcd.setCursor(12,0);
    if (capturePics){
        lcd.print("pic");
    }
    else{
        lcd.print("vid");        
    }
    
    lcd.setCursor(12,1);
    lcd.print("edit");

    // wait for user input
    buttonWait(Button1_Pin);
}

bool sendSettings(){
    bool confirm = false;
    // send setting string and wait for confirmation, repeat...
    while (!confirm){
        Serial.print("$relayOnTime:");
        Serial.println(relayOnTime);
        confirm = getConfirm();
        delay(100);
    }
    delay(100);
    confirm = false;
    while (!confirm){
        Serial.print("$capturePics:");
        Serial.println(capturePics);
        confirm = getConfirm();
        delay(100);
    }
    delay(100);
    confirm = false;
    while (!confirm){
        Serial.print("$picsTotal:");
        Serial.println(picsTotal);
        confirm = getConfirm();
        delay(100);
    }
    delay(100);
    confirm = false;
    while (!confirm){
        Serial.print("$scanObjectBottom:");
        Serial.println(scanObjectBottom);
        confirm = getConfirm();
        delay(100);
    }
    delay(100);
    confirm = false;
    while (!confirm){
        Serial.print("$camPositions:");
        Serial.println(camPositions);
        confirm = getConfirm();
        delay(100);
    }
    delay(100);
    confirm = false;
    while (!confirm){
        Serial.print("$picInterval:");
        Serial.println(picInterval);
        confirm = getConfirm();
        delay(100);
    }
    delay(100);
    return(confirm);
    
    
//     Serial.print();
//     Serial.println();
//     Serial.print();
//     Serial.println();
//     Serial.print();
}
    
void sendStatus(){
    Serial.print("$camPosition:");
    Serial.println(camPosition);
    Serial.print("$picsTaken:");
    Serial.println(picsTaken);
    Serial.print("$totalPicsTaken:");
    Serial.println(totalPicsTaken);
//     Serial.print();
//     Serial.println();
}


void pad(int value){
    // accepts number up to 3 digits, pads the lcd with spaces
    if(value < 100){
        lcd.print(" ");
    }
    if(value < 10){
        lcd.print(" ");
    }
}



void editSettings(){
    Serial.println("Edit Settings");
    lcd.setCursor(12,1);
    lcd.print("store");
    buttonWait(Button1_Pin);

}

//******* LCD Display Draw Function **************
void lcdDrawProgress() {
    int rotationProgress = 0;   // percentage of 1 rotation
    int totalProgress = 0;    // percentage of the total capture
    
    Serial.println("Calculate the progress");
    
    rotationProgress = 100 * picsTaken / (picsTotal / (camPositions + scanObjectBottom));
    totalProgress = 100 * (picsTaken + ((picsTotal / (camPositions + scanObjectBottom)) * camPosition)) / picsTotal;
    
    Serial.print("pic: ");
    Serial.print(picsTaken);
    Serial.print("    rotation: ");
    Serial.print(rotationProgress);
    Serial.print("    total: ");
    Serial.println(totalProgress);
    
    lcd.setCursor(0,0);
    pad(totalProgress);
    lcd.print(totalProgress);
    lcd.setCursor(3,0);
    lcd.print("% total");
  
    // the progress of this rotation    
    
    lcd.setCursor(0,1);
    pad(rotationProgress);
    lcd.print(rotationProgress);
    lcd.setCursor(3,1);
    lcd.print("% at pos");
 
    // the camera tilt position
    /*lcd.setCursor(11,1);
    */lcd.print("pos");
    lcd.setCursor(15,1);
    //TODO: padding 
    lcd.print(camPosition);
    
}


void takePhoto(){
    //Serial.print(".");
    lcd.setCursor(15,0);
    lcd.print("x");
    delay(100);
    lcd.setCursor(15,0);
    lcd.print(" ");
}

//******* Do the Scan **************
void performScan() {
    lcd.clear();
    
    // calculate pic intervals (in milliseconds) from settings
    picInterval = relayOnTime * 1000 * camPositions / (picsTotal + scanObjectBottom);
    
    
    lcd.setCursor(0,0);
    lcd.print("set the camera");
    lcd.setCursor(0,1);
    lcd.print("to position ");
    lcd.print(camPosition + 1); 
    delay(1000);

    // wait for user input to begin the scan
    buttonWait(Button1_Pin);         
    lcd.clear();
    
    Serial.println("BEGIN SCAN");
    Serial.print("Camera Positions:   ");
    Serial.println(camPositions);
    Serial.print("Rotation Duration:  ");
    Serial.print(relayOnTime);
    Serial.println("s");
    Serial.print("Number of Pictures: ");
    Serial.println(picsTotal);
    Serial.print("Picture Interval:   ");
    Serial.print(picInterval);
    Serial.println("ms");

    
    // loop through camera positions
    while (camPosition < (camPositions + scanObjectBottom)){
        Serial.print("Begin rotation at camera position ");
        Serial.println(camPosition);
        Serial.print("Start: ");
        totalPicsTaken = totalPicsTaken + picsTaken;     //total the pics taken
        picsTaken = 0;                                   //reset pic counter for the current rotation
        // now do one full turn of the table
        while (picsTaken < picsTotal / camPositions){
            digitalWrite(Relay1_Pin, HIGH);   // Turn ON the turntable relay 
            digitalWrite(LedPin, HIGH);       // and turn on the status LED
            // TODO: trigger the camera to take a photo then wait for specified time
            
            while (picsTaken < picsTotal / camPositions){
                if (capturePics == 1){
                    picsTaken ++;
                    takePhoto();
                }
                // calculate the progress
                lcdDrawProgress();   // update the display
                delay(picInterval);
            }
            
        }
        digitalWrite(Relay1_Pin, LOW);   // Turn OFF the relay 
        digitalWrite(LedPin, LOW);       // and turn OFF the status LED
        camPosition ++;                  // go to the next camera position
        if (camPosition < camPositions){
            lcd.clear();                     // give instructions via the display
            lcd.setCursor(0,0);
            lcd.print("raise the camera");
            lcd.setCursor(0,1);
            lcd.print("to position ");
            lcd.print(camPosition + 1);        
            buttonWait(Button1_Pin);         // wait for user input
        }
        else{
            if (scanObjectBottom){
                lcd.clear();                     // give instructions via the display
                lcd.setCursor(0,0);
                lcd.print("turn the object");
                lcd.setCursor(0,1);
                lcd.print("scan underside ");
//                 buttonWait(Button1_Pin);         // wait for user input
            
            }
        
        
        }
        lcd.clear();
    }
    // finished all of the rotations
    Serial.println("FINISHED");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("finished");
    delay(25000);
}


void loop(){
    bool confirm = false;
    bool beginScan = false;
    
    char commandChar[];
    String commandString;
    
    
    lcd.clear();
    // look for a serial connection
    lcd.setCursor(0,0);
    lcd.print("testing camera");
    lcd.setCursor(0,1);
    lcd.print("connection");
    
    while (!confirm){
        confirm = connectSerial();
        if (confirm){
            lcd.clear();
            lcd.print("Camera connected");
            lcd.setCursor(0,1);
            lcd.print("press the button");
            buttonWait(Button1_Pin);
        }
        else{
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("check USB");
            lcd.setCursor(0,1);
            lcd.print("cable");
        }
    }
    confirm = false;

    // Enter interactive mode, break to begin scan
    while(!beginScan){
        commandString = Serial.readString();
        commandString.remove(3);
        commandString.toCharArray(commandChar, 4);  // don't know why this needs to be length + 1

        lcd.clear();
        lcd.setCursor(4,0);
        lcd.print(responseChar);
        if (strcmp(responseChar, "@OK") == 0){
            Serial.println("response OK, continue");
            return(true);
        }
        else{
            blink(10,100, LedPin);
            Serial.print("Response was :");
            Serial.println(responseChar);
            return(false);
        }
        
        lcd.print("Send Settings");
        confirm = sendSettings();
        lcd.clear();
        lcd.setCursor(0,0);
        // confirm that the camera has received the sttings and is ready
        
    }
        
        //***** menu option - show settings *****//
        //showSettings();

        //***** menu option - edit settings *****//
        //editSettings();
        
        // allow user to change scan settings
        showSettings();
        
    //**********    do the scan    **********//
    performScan();
    
    // wait for user input and start again
    buttonWait(Button1_Pin);
    


}
