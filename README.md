# ttControl

Arduino sketch to control a microwave oven turntable
Python program to interface with the Arduino and control a USB camera
Together they create a dataset to be input into a photogrammetry pipeline


Python3 libraries
    $ sudo apt install python3-opencv
    (or)	$ pip3 install opencv-python
    $ sudo apt install python3-serial

Arduino libraries
	liquidcrystal_i2c
