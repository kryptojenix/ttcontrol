#!/usr/bin/python3
import cv2
import time
import serial
#import errno

global cameraPort
cameraPort = 0
global serial_port


class usbCamera:
    """ Camera to interact with during the scan process"""

    def __init__(self, usb_ID, res_available, lense_properties, camera_port):
        self.usb_ID = usb_ID
        self.res_available = res_available
        self.lense_properties = lense_properties
        self.camera_port = camera_port

    def getID(self):
        return self.usb_ID

    def listResAvailable(self):
        return self.res_available

    def getLenseProperties(self):
        pass
        # lense properties are important for photogrammetry

        # check if we already know them

        # if not, check the database

    def setupCamera(self, camera_port):
        cam = cv2.VideoCapture(cameraPort)
        time.sleep(0.1)

        cam.set(3, 640  ) # width        
        cam.set(4, 480  ) # height       
        cam.set(10, 120  ) # brightness     min: 0   , max: 255 , increment:1  
        cam.set(11, 50   ) # contrast       min: 0   , max: 255 , increment:1     
        cam.set(12, 70   ) # saturation     min: 0   , max: 255 , increment:1
        cam.set(13, 13   ) # hue         
        cam.set(14, 50   ) # gain           min: 0   , max: 127 , increment:1
        cam.set(15, -3   ) # exposure       min: -7  , max: -1  , increment:1
        cam.set(17, 5000 ) # white_balance  min: 4000, max: 7000, increment:1
        cam.set(28, 0    ) # focus          min: 0   , max: 255 , increment:5


    def testCamera(self):
        cv2.namedWindow("preview")
        cam = cv2.VideoCapture(0)

        if cam.isOpened(): # try to get the first frame
            rval, frame = cam.read()
        else:
            rval = False

        while rval:
            cv2.imshow("preview", frame)
            rval, frame = cam.read()
            key = cv2.waitKey(20)
            if key == 27: # exit on ESC
                break
        cv2.destroyWindow("preview")

    def getVideo():
        pass

    def getFrame():

        return_value, image = cam.read()
        cv2.imwrite("opencv.jpg", image)


class arduinoTT:
    """The turntable we are interfacing with"""
    def __init__(self,serial_port, baud_rate, relay_on_time, capture_pics):

        # these are the options that pySerial needs
        self.serial_port = serial_port
        self.baud_rate = baud_rate
        self.time_out = time_out
        self.parity = parity
        self.stopbits = stopbits
        self.bytesize = bytesize

        # the following properties are user selectable in the arduino UI and
        # can be got from the serial interface

        self.relay_on_time          # seconds for a complete rotation - user configurable
        self.capture_pics          # 0 = video, 1 = stills - how to trigger camera 
        self.pics_total            # set the "resolution" of the scan
        self.cam_positions         # how many camera angles to capture
        self.cam_position          # keeps track of camera positions
        self.pic_interval          # milliseconds, calculated - time between pics
        self.pics_taken            # used to calculate the rotation progress
        self.total_pics_taken       # used to calculate the total progress
        self.pics_per_revolution    # calculated from total pics

    def connectSerial(self, serial_port, baud_rate, time_out):
        pass
        # make a serial connection


    def getScanSettings(self):
        # this gets the settings that will be used for the scan
        return "{} {} {} {} {}".format(self.relay_on_time, self.capturePics, self.picsTotal, self.camPositions, self.picInterval)



def main():
    
    #usb_ID = "045e:074a"
    #webcam = usbCamera(usb_ID, res_available, lense_properties, camera_port)
    
    #webcam.setupCamera()
    #webcam.testCamera()
    
    show_webcam()
    
def show_webcam(mirror=False):
    """
    Simply display the contents of the webcam with optional mirroring using OpenCV 
    via the new Pythonic cv2 interface.  Press <esc> to quit.
    """
    https://gist.github.com/tedmiston/6060034
    cam = cv2.VideoCapture(0)
    while True:
        ret_val, img = cam.read()
        if mirror: 
            img = cv2.flip(img, 1)
        cv2.imshow('my webcam', img)
        if cv2.waitKey(1) == 27: 
            break  # esc to quit
    cv2.destroyAllWindows()

    
def connect_camera():
    """
    connects to the arduino via serial
    """
    
    replyPrefix = "$"
    confirm = "@OK\r\n"

    # check for serial connection
    while True:
        print("connecting to serial")
        try:
            arduinoPort = serial.Serial(
                port = '/dev/ttyUSB0', 
                baudrate = 9600, 
                timeout = 5
                )
                #parity = x, 
                #stopbits = y, 
                #bytesize = z
        except Exception as e:
            print(e)
            if e.errno == 16:
                print("close the arduino serial monitor to continue")
            time.sleep(10)
        except KeyboardInterrupt:
            break
        else:
            print("connected to ", arduinoPort.name)
            arduinoPort.flushInput()
            break
    
    # poll the arduino until it responds
    #it will be rebooting and running its setup function
    time.sleep(5)
    expecting = "$hello\r\n"
    while True:
        try:
            arduinoLine = arduinoPort.readline()
            print(arduinoLine)
            if(arduinoLine == expecting.encode('UTF-8')):
                print("Expecting ", expecting.encode('UTF-8'), " and got ", arduinoLine)
                break
        except KeyboardInterrupt:
            break
        except:
            print("Timeout waiting for turntable")
        else:
            print(arduinoLine)
            time.sleep(2)

    print("sending confirmation.....")
    arduinoPort.write(confirm.encode('UTF-8'))


    # request status from arduino turntable
    serialCommand = "@STATUS\r\n"
    arduinoPort.write(serialCommand.encode('UTF-8'))
    
    # get a response and store the value
    while True:
        try:
            arduinoLine = str(arduinoPort.readline())
            print(arduinoLine)
            print(type(arduinoLine))
            #print(str(arduinoLine)[2])
            if(arduinoLine[2] == "$"):
                print("command : ", arduinoLine)
                
                break
            else:
                print("ignoring non comms output")
        except KeyboardInterrupt:
            break
        #except:
            #print("Timeout waiting for STATUS")
            #time.sleep(2)
    
    
    
    
    serialCommand = "@SETTINGS"
    # get settings from the turntable
    arduinoPort.write(serialCommand.encode())
    
    
    #if cam.isOpened(): # try to get the first frame
        #rval, frame = cam.read()
    #else:
        #rval = False

if __name__ == '__main__':
    main()

